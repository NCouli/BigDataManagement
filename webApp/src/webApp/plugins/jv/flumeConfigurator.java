package webApp.plugins.jv;

import java.io.File;
import java.io.Writer;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedWriter;

public class flumeConfigurator {
	public void flumeCreator(int brokers) throws IOException {
		try {
			String flumeHome = System.getenv("FLUME_HOME") + "/conf/twitter.conf";
			File flumeFile = new File(flumeHome);
			Writer flumeWriter = new BufferedWriter(new FileWriter(flumeFile, false));
			flumeWriter.write(dataLoader(brokers));
			flumeWriter.flush();
			flumeWriter.close();
		} catch (IOException e) {
			e.getStackTrace();
		}
	}

	private static String confBrokData(int brokers) {
		String tmp = "";
		int port = 9092;
		for (int i = 0; i < brokers; i++) {
			tmp += " localhost:" + (port + i);
			if (i < brokers - 1)
				tmp += ",";
		}
		tmp += "\n";
		return tmp;
	}

	private static String dataLoader(int brokers) {
		return "# List of sources, sinks and channels\n" +
				"TwitterAgent.sources = Kafka\n" +
				"# Fan out flow: multiple channels and multiple sinks\n" +
				"TwitterAgent.sinks = HDFS-1 HDFS-2\n" +
				"TwitterAgent.channels = MemChannel-1 MemChannel-2\n" +
				"# Kafka attributes\n" +
				"TwitterAgent.sources.Kafka.type = org.apache.flume.source.kafka.KafkaSource\n" +
				"TwitterAgent.sources.Kafka.kafka.bootstrap.servers =" + confBrokData(brokers) + "\n\n" +
				"TwitterAgent.sources.Kafka.kafka.topics = my_twitter\n" +
				"TwitterAgent.sources.Kafka.kafka.consumer.group.id = flume\n" +
				"TwitterAgent.sources.Kafka.kafka.consumer.timeout.ms = 100\n" +
				"# Timestamp interceptors for event headers\n" +
				"TwitterAgent.sources.Kafka.interceptors = i1\n" +
				"TwitterAgent.sources.Kafka.interceptors.i1.type = timestamp\n" +
				"# HDFS event configs\n" +
				"TwitterAgent.sinks.HDFS-1.type = hdfs\n" +
				"TwitterAgent.sinks.HDFS-1.hdfs.path = hdfs://localhost:9000/twitter_data/%{topic}/%y-%m-%d\n" +
				"TwitterAgent.sinks.HDFS-1.hdfs.fileType = DataStream\n" +
				"TwitterAgent.sinks.HDFS-1.hdfs.writeFormat = Text\n" +
				"TwitterAgent.sinks.HDFS-1.hdfs.batchSize = 10000\n" +
				"TwitterAgent.sinks.HDFS-1.hdfs.rollInterval = 300\n" +
				"TwitterAgent.sinks.HDFS-1.hdfs.rollSize = 0\n" +
				"TwitterAgent.sinks.HDFS-1.hdfs.rollCount = 0\n" +
				"TwitterAgent.sinks.HDFS-1.hdfs.fileSuffix = .avro\n" +
				"# Avro Serializer\n" +
				"TwitterAgent.sinks.HDFS-1.serializer = org.apache.flume.sink.hdfs.AvroEventSerializer$Builder\n" +
				"TwitterAgent.sinks.HDFS-1.serializer.schemaURL = file:///tweet.avsc\n\n" +
				"TwitterAgent.sinks.HDFS-2.type = logger\n" +
				"TwitterAgent.channels.MemChannel-1.type = memory\n" +
				"TwitterAgent.channels.MemChannel-2.type = memory\n\n" +
				"# List of channels for source\n" +
				"TwitterAgent.sources.Kafka.channels = MemChannel-1 MemChannel-2\n\n" +
				"TwitterAgent.channels.MemChannel-1.capacity = 10000\n" +
				"TwitterAgent.channels.MemChannel-1.transactionCapacity = 10000\n" +
				"TwitterAgent.channels.MemChannel-2.capacity = 10000\n" +
				"TwitterAgent.channels.MemChannel-2.transactionCapacity = 10000\n\n" +
				"TwitterAgent.sinks.HDFS-1.channel = MemChannel-1\n" +
				"TwitterAgent.sinks.HDFS-2.channel = MemChannel-2 \n\n" +
				"# Not neccessary cause default value is \"replicating\"\n" +
				"TwitterAgent.sources.Kafka.selector.type = replicating\n";
	}
}
