package webApp.plugins.jv;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class datFormater {

	private Configuration conf;
	private FileSystem fs;

	public datFormater() throws IOException {
		conf = new Configuration();
		conf.set("fs.defaultFS", "hdfs://localhost:9000");
		conf.addResource(new Path("/HADOOP_HOME/conf/core-site.xml"));
		conf.addResource(new Path("/HADOOP_HOME/conf/hdfs-site.xml"));
		fs = FileSystem.get(conf);
	}

	public void pullNPush(String path) throws IOException {
		writeFileToLS(path);
		writeFileToHDFS(path);
		System.out.println("Data for KNN are stored in HDFS");
	}

	public void writeFileToLS(String path) {
		FSDataInputStream in = null;
		OutputStream output = null;
		try {
			File file = new File("C:/tmp/mapRed.txt");
			file.createNewFile();
			output = new FileOutputStream("C:/tmp/mapRed.txt");
			// Input file path
			Path inFile = new Path("hdfs://localhost:9000/twitter_data/MapReduced/" + path + "/part-00000");
			// Check if file exists at the given location
			if (!fs.exists(inFile)) {
				System.out.println("Input file not found");
				throw new IOException("Input file not found");
			}
			// open and read from file
			in = fs.open(inFile);
			// displaying file content on terminal
			byte buffer[] = new byte[256];

			int bytesRead = 0;
			while ((bytesRead = in.read(buffer)) > 0) {
				output.write(buffer, 0, bytesRead);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// Closing streams
			try {
				if (in != null)
					in.close();
				if (output != null)
					output.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void writeFileToHDFS(String path) throws IOException { // Save Data to HDFS
		// Create a path
		String fileName = "serializedData.dat";
		Path hdfsWritePath = new Path("/twitter_data/clusterInput/" + path + "/" + fileName);
		FSDataOutputStream fsDataOutputStream = fs.create(hdfsWritePath, true);
		File tmpFile = new File("C:/tmp/mapRed.txt");
		try (BufferedReader br = new BufferedReader(new FileReader(tmpFile))) {
			BufferedWriter bufferedWriter = new BufferedWriter(
					new OutputStreamWriter(fsDataOutputStream, StandardCharsets.UTF_8));
			String st;
			while ((st = br.readLine()) != null)
				bufferedWriter.write(st + "\n");

			bufferedWriter.close();
		}
		fs.close();
		//File file = new File("C:/tmp/mapRed.txt");
		//file.delete();
	}
}
