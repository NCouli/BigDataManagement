package webApp.plugins.jv;

import java.io.File;
import java.io.FileWriter;

import org.apache.avro.Schema;
import org.apache.avro.mapred.FsInput;
import org.apache.avro.io.DatumReader;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.Configuration;

public class avroDes {
    public void parserAvroHDFS(String pathHDFS) throws Exception {

        File file = new File("C:/tmp/serializedData.txt");
        file.createNewFile();
        FileWriter output = new FileWriter("C:/tmp/serializedData.txt", false);
        Configuration conf = new Configuration();
        FsInput in = new FsInput(new Path(pathHDFS), conf);

        Schema schema = new Schema.Parser().parse(new File("tweet.avsc"));
        DatumReader<GenericRecord> datumReader = new GenericDatumReader<GenericRecord>(schema);
        DataFileReader<GenericRecord> dataFileReader = new DataFileReader<GenericRecord>(in, datumReader);
        GenericRecord location = null;

        String[] states = statesData(false);
        String[] postal = statesData(true);
        String data;
        if (dataFileReader.hasNext())
            while (true) {
                data = "51";
                location = dataFileReader.next(location);
                for (int i = 0; i < states.length; i++) // If a state/postal name is found inside a registry
                    if (location.get("location").toString().contains(states[i]) || location.get("location").toString().contains(postal[i]))
                        data = location.get("location").toString().replace(location.get("location").toString(), String.valueOf(i + 1));
                   
                output.write(data);
                if (!dataFileReader.hasNext()) // Exit condition
                    break;
                output.write("\n");
            }

        dataFileReader.close();
        output.close();
        System.out.println("Data Parsed And Saved Successfully!");
    }

    private static String[] statesData(boolean postal) {
        // https://www.sos.arkansas.gov/education/arkansas-history/history-of-the-flag/order-of-states-admission
        // https://www.scouting.org/resources/los/states/
        if (!postal)
            return new String[] {
                    "Delaware", "Pennsylvania", "New Jersey", "Georgia", "Connecticut",
                    "Massachusetts", "Maryland", "South Carolina", "New Hampshire", "Virginia",
                    "New York", "North Carolina", "Rhode Island", "Vermont", "Kentucky",
                    "Tennessee", "Ohio", "Louisiana", "Indiana", "Mississippi",
                    "Illinois", "Alabama", "Maine", "Missouri", "Arkansas",
                    "Michigan", "Florida", "Texas", "Iowa", "Wisconsin",
                    "California", "Minnesota", "Oregon", "Kansas", "West Virginia",
                    "Nevada", "Nebraska", "Colorado", "North Dakota", "South Dakota",
                    "Montana", "Washington", "Idaho", "Wyoming", "Utah",
                    "Oklahoma", "New Mexico", "Arizona", "Alaska", "Hawaii"
            };
        return new String[] {
                "DE", "PA", "NJ", "GA", "CT",
                "MA", "MD", "SC", "NH", "VA",
                "NY", "NC", "RI", "VT", "KY",
                "TN", "OH", "LA", "IN", "MS",
                "IL", "AL", "ME", "MO", "AR",
                "MI", "FL", "TX", "IA", "WI",
                "CA", "MN", "OR", "KS", "WV",
                "NV", "NE", "CO", "ND", "SD",
                "MT", "WA", "ID", "WY", "UT",
                "OK", "NM", "AZ", "AK", "HI"
        };
    }
}
