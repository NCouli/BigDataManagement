package webApp.plugins.jv;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;

import org.apache.hadoop.mapred.FileOutputFormat;

public class mRunner {
	public void runner() throws IOException {
		JobConf conf = new JobConf(mRunner.class);
		conf.setJobName("locationFinder");

		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(IntWritable.class);

		conf.setMapperClass(mMapper.class);
		conf.setCombinerClass(mReducer.class);
		conf.setReducerClass(mReducer.class);

		conf.setInputFormat(TextInputFormat.class);
		conf.setOutputFormat(TextOutputFormat.class);
		conf.set("mapred.textoutputformat.separator", " ");

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss"); // format:
																					// year-month-day:hours-minutes-seconds
		String arguments[] = new String[2];
		arguments[0] = "C:/tmp/serializedData.txt"; // input stream
		arguments[1] = "hdfs://localhost:9000/twitter_data/MapReduced/" + dtf.format(LocalDateTime.now()).toString(); // output
																														// stream
		FileInputFormat.setInputPaths(conf, new Path(arguments[0]));
		FileOutputFormat.setOutputPath(conf, new Path(arguments[1]));
		try {
			JobClient.runJob(conf);
			File file = new File("C:/tmp/serializedData.txt"); // remove temporary serialized file
			file.delete();
			System.out.println("Job was successful");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Job was not successful");
		}
	}
}
