package webApp.plugins.jv;

import java.io.File;
import java.io.Writer;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedWriter;

public class avroSchema {
	public void avroSchemaCreator(String[] fields) throws IOException {
		try {
			File avscFile = new File("C:/tweet.avsc");
			Writer avscWriter = new BufferedWriter(new FileWriter(avscFile, false));

			avscWriter.write("{\n\t"
					+ "\"namespace\": \"kafka.tweets\",\n\t"
					+ "\"type\": \"record\",\n\t"
					+ "\"name\": \"tweet\",\n\t"
					+ "\"fields\": [\n");

			for (int i = 0; i < fields.length;) {
				avscWriter.write("\t\t{\"name\": \"" + fields[i] + "\", \"type\": \"" + fields[i + 1] + "\"}");
				avscWriter.flush();
				if (i + 2 < fields.length) {
					avscWriter.write(",");
					avscWriter.flush();
				}
				avscWriter.write("\n");
				avscWriter.flush();
				i += 2;
			}

			avscWriter.write("\t]\n}");
			avscWriter.flush();
			avscWriter.close();

		} catch (IOException e) {
			e.getStackTrace();
		}
	}
}
