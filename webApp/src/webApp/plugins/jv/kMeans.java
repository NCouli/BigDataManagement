package webApp.plugins.jv;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.Configuration;
import org.apache.mahout.clustering.kmeans.KMeansDriver;
import org.apache.mahout.clustering.conversion.InputDriver;
import org.apache.mahout.clustering.kmeans.RandomSeedGenerator;
import org.apache.mahout.common.distance.DistanceMeasure;
import org.apache.mahout.common.distance.EuclideanDistanceMeasure;
import org.apache.mahout.utils.clustering.ClusterDumper;

public class kMeans {
    public void RunKmeans(String path, int clsSize, double convergence, int maxIterations) throws Exception {

        Path input = new Path("hdfs://localhost:9000/twitter_data/clusterInput/" + path + "/");
        Path output = new Path("hdfs://localhost:9000/twitter_data/Mahout/" + path + "/");

        Configuration conf = new Configuration();
        run(conf, input, output, new EuclideanDistanceMeasure(), clsSize, convergence, maxIterations);
    }

    public static void run(Configuration conf, Path input, Path output, DistanceMeasure measure, int k,
            double convergence, int maxIterations) throws Exception {

        Path directoryContainingConvertedInput = new Path(output, "KmeansOutputData");
        InputDriver.runJob(input, directoryContainingConvertedInput, "org.apache.mahout.math.RandomAccessSparseVector");

        Path clusters = new Path(output, "random-seeds");
        clusters = RandomSeedGenerator.buildRandom(conf, directoryContainingConvertedInput, clusters, k, measure);

        KMeansDriver.run(conf, directoryContainingConvertedInput, clusters, output, convergence, maxIterations,
                true, 0.0, false);

        Path outGlob = new Path(output, "clusters-*-final");
        Path clusteredPoints = new Path(output, "clusteredPoints");

        ClusterDumper clusterDumper = new ClusterDumper(outGlob, clusteredPoints);
        clusterDumper.printClusters(null);
    }
}
