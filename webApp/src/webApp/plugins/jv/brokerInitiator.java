package webApp.plugins.jv;

import java.io.IOException;
import java.io.File;
import java.io.Writer;
import java.io.BufferedWriter;
import java.io.FileWriter;

public class brokerInitiator {
	public void brokerCreator(int brokCnt) throws IOException {
		try {

			String kafkaPath = createBrokerDir();
			File batFile = new File(kafkaPath + "brokerIni.bat");
			Writer batWriter = new BufferedWriter(new FileWriter(batFile, false));

			batWriter.write("@echo off\ncd %KAFKA_HOME%\n");
			batWriter.flush();

			for (int i = 0; i < brokCnt; i++) {
				batWriter.write("start .\\bin\\windows\\kafka-server-start.bat " +
						".\\config\\serverConfig\\server-" + i + ".properties\n");
				batWriter.flush();

				File serverFile = new File(kafkaPath + "server-" + i + ".properties");
				Writer serverWriter = new BufferedWriter(new FileWriter(serverFile, false));
				serverWriter.write(fileData(i));
				serverWriter.flush();
				serverWriter.close();
			}
			batWriter.close();

		} catch (IOException e) {
			e.getStackTrace();
		}
	}

	private String createBrokerDir() {
		String kafkaHome = System.getenv("KAFKA_HOME") + "/config/serverConfig"; // Locate kafka's Environment variable
		File theDir = new File(kafkaHome);
		if (!theDir.exists()) // Checks if "serverConfig" directory exists
			theDir.mkdirs();

		return kafkaHome + "/"; // Returns "absolute" path
	}

	private String fileData(int i) {
		int port = 9092 + i;
		return "broker.id=" + i + "\n" +
				"listeners=PLAINTEXT://:" + port + "\n" +
				"num.network.threads=3\n" +
				"num.io.threads=8\n" +
				"socket.send.buffer.bytes=102400\n" +
				"socket.receive.buffer.bytes=102400\n" +
				"socket.request.max.bytes=104857600\n" +
				"log.dirs=" + System.getenv("KAFKA_HOME") + "/kafka-logs-" + i + "\n" +
				"num.partitions=1\n" +
				"num.recovery.threads.per.data.dir=1\n" +
				"offsets.topic.replication.factor=1\n" +
				"transaction.state.log.replication.factor=1\n" +
				"transaction.state.log.min.isr=1\n" +
				"log.retention.hours=168\n" +
				"log.segment.bytes=1073741824\n" +
				"log.retention.check.interval.ms=300000\n" +
				"zookeeper.connect=localhost:2181\n" +
				"zookeeper.connection.timeout.ms=18000\n" +
				"group.initial.rebalance.delay.ms=0\n";
	}
}