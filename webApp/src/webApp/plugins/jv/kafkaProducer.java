package webApp.plugins.jv;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.google.common.collect.Lists;

import com.twitter.hbc.core.Client;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;

import com.twitter.hbc.httpclient.auth.OAuth1;
import com.twitter.hbc.httpclient.auth.Authentication;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;

import com.twitter.bijection.Injection;
import com.twitter.bijection.avro.GenericAvroCodecs;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class kafkaProducer {

	public void runProducer(int brokers, String[] twitterKeys, String[] keywords, int msgCnt)
			throws IOException {

		Properties props = propertiesData(brokers);

		Schema schema = new Schema.Parser().parse(new File("C:/tweet.avsc"));
		Injection<GenericRecord, byte[]> recordInjection = GenericAvroCodecs.toBinary(schema);
		JSONParser jsonParser = new JSONParser();

		Producer<String, byte[]> producer = null;
		try {
			producer = new KafkaProducer<>(props);

			BlockingQueue<String> queue = new LinkedBlockingQueue<String>(10000);
			StatusesFilterEndpoint endpoint = new StatusesFilterEndpoint();
			endpoint.trackTerms(Lists.newArrayList(keywords[0], keywords[1], keywords[2]));
			Authentication auth = new OAuth1(twitterKeys[0], twitterKeys[1], twitterKeys[2], twitterKeys[3]);

			Client client = new ClientBuilder()
					.hosts(Constants.STREAM_HOST)
					.endpoint(endpoint)
					.authentication(auth)
					.processor(new StringDelimitedProcessor(queue))
					.build();

			client.connect();

			int i = 0;
			while (i < msgCnt) {
				String msg = queue.take();
				GenericData.Record avroRecord = new GenericData.Record(schema);
				Map user = (Map) ((JSONObject) jsonParser.parse(msg)).get("user");
				if (user.get("location") != null) {
					avroRecord.put("location", user.get("location").toString());
					byte[] bytes = recordInjection.apply(avroRecord);
					ProducerRecord<String, byte[]> record = new ProducerRecord<>("my_twitter", bytes);
					producer.send(record);
					System.out.println(i + ")Sent:" + record);
					i++;
				}
			}

			producer.close();
			client.stop();
			System.out.println("Done!");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	private String brockersPorts(int brokers) { // Creates a string with all the brokers active ports
		int prtNu = 9091;
		String port = "";
		while (true) { // string builder
			prtNu++;
			port += "localhost:" + prtNu;
			if (prtNu == brokers + 9091)
				break;
			port += ", ";
		}
		if (port == "") // Fail safe
			port = "localhost:9092";
		return port;
	}

	private Properties propertiesData(int brokers) { // topic with one partition data
		Properties props = new Properties();
		props.put("bootstrap.servers", brockersPorts(brokers));
		props.put("acks", "all");
		props.put("retries", 0);
		props.put("batch.size", 16384);
		props.put("linger.ms", 1);
		props.put("buffer.memory", 33554432);
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", "org.apache.kafka.common.serialization.ByteArraySerializer");
		return props;
	}
}
