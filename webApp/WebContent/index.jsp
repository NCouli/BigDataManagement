<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import = "webApp.plugins.jv.brokerInitiator"%>
<%@ page import = "webApp.plugins.jv.kafkaProducer"%>	
<%@ page import = "webApp.plugins.jv.avroDes"%>	
<%@ page import = "webApp.plugins.jv.mRunner"%>
<%@ page import = "webApp.plugins.jv.kMeans"%>
<%@ page import = "webApp.plugins.jv.datFormater"%>
<%@ page import = "webApp.plugins.jv.avroSchema"%>
<%@ page import = "webApp.plugins.jv.flumeConfigurator"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Big Data Management</title>
</head>
<body> 
	<%
		int brokers = 1;
		
		String[] twitterKeys = new String[] { 
		"", // API Key
		"",	// API Key Secret
		"",	// Access Token
		""	// Access Token Secret
		};
		
		String []keywords = new String[]{"Crypto", "nfts", "Metaverse"};
			
		String pathHDFS = "hdfs://localhost:9000/twitter_data/my_twitter/";

		String timestamp = "";
			
		if(request.getParameter("btnBrokerIni") != null){
			
			if(request.getParameter("brokers") != null)
				if((brokers = Integer.parseInt(request.getParameter("brokers"))) < 0)
					brokers = 1;
			
			brokerInitiator br = new brokerInitiator();
			br.brokerCreator(brokers);
			
			avroSchema avr = new avroSchema();
			avr.avroSchemaCreator(new String[]{"location", "string"});
			
			flumeConfigurator flm = new flumeConfigurator();
			flm.flumeCreator(brokers);
			System.out.println("All necessary files have been created");

		}
		
		if(request.getParameter("btnKafkaProd") != null){
			if(request.getParameter("key1") !="")
				keywords[0] = request.getParameter("key1");
			if(request.getParameter("key2") != "")
				keywords[1] = request.getParameter("key2");
			if(request.getParameter("key3") !="")
				keywords[2] = request.getParameter("key3");
			
			kafkaProducer pr = new kafkaProducer();
			pr.runProducer(brokers, twitterKeys, keywords, 200);

		}
		
		if(request.getParameter("btnPreproc") != null){
			avroDes ad = new avroDes();
			if(request.getParameter("pathHDFS") != "");
				pathHDFS = request.getParameter("pathHDFS");
			ad.parserAvroHDFS(pathHDFS);
		}

		if(request.getParameter("btnMapRed") != null){
			mRunner runner = new mRunner();
			runner.runner();
		}
		
		if(request.getParameter("process4KNN") != null){
			datFormater datMaker = new datFormater();
			if(request.getParameter("folder4Formating") != "");
				timestamp = request.getParameter("folder4Formating");
			datMaker.pullNPush(timestamp);
		}		
		
		int clsNu = 2;
		double cnvDelta = 0.5;
		int maxIter = 10;
		
		if(request.getParameter("dataAnalysis") != null){
			kMeans knn = new kMeans();
						
			if(request.getParameter("clsNu") != ""){
				clsNu = Integer.parseInt(request.getParameter("clsNu"));
			}
			if(request.getParameter("cnvDelta") != "")
				cnvDelta = Double.parseDouble(request.getParameter("cnvDelta"));
			if(request.getParameter("maxIter") != "")
				maxIter = Integer.parseInt(request.getParameter("clsNu"));
			
			if(request.getParameter("dataKNN") != ""){
				timestamp = request.getParameter("dataKNN");
				knn.RunKmeans(timestamp, clsNu, cnvDelta, maxIter);
			}
		}
		
	%>
	<h1>Big Data Management</h1>
    <form method="POST">
    	<h4>Provide Kafka Brokers count (Use only 1 because WebApp might become unstable)</h4>
    	Brokers #: <input type = "text" name = "brokers" size="5" /><br>
    	<input type="submit" name="btnBrokerIni" value="Initiate Brokers">
    </form>
    <br>
    Don't forget to start HADOOP, Kafka Zookeeper, Kafka Server and Flume
    <h4>Kafka Producer</h4>
    <form method="POST">
		Provide 3 keywords (Default 3: "Crypto", "nfts", "Metaverse"):<br>
		Keyword 1: <input type = "text" name = "key1" size="50" /> <br>
		Keyword 2: <input type = "text" name = "key2" size="50" /> <br>
		Keyword 3: <input type = "text" name = "key3" size="50" /> <br>
    	<input type="submit" name="btnKafkaProd" value="Produce Data">
    </form>
	<h4>Data Processing (Wait approximately 5 minutes)</h4>
	<form method="POST">
		HDFS path:  <input type = "text" name = "pathHDFS" size="80" value = "hdfs://localhost:9000/twitter_data/my_twitter/"/> <br>
    	<input type="submit" name="btnPreproc" value="Process Data">
    </form>
	<h4>MapReduce</h4>
	<form method="POST">
    <input type="submit" name="btnMapRed" value="MapReduce">
    </form>
    <h4>Process And Store Data on HDFS For Data Analysis Using K-Means Cluster</h4>
    <form method="POST">
    	Folder Name with Data To Process: <input type = "text" name = "folder4Formating" size="50" /> <br>
    	<input type="submit" name="process4KNN" value="Process Data"><br>
    </form>
    <form method="POST">
    	Folder with Data To Process: <input type = "text" name = "dataKNN" size="50" /> <br>
    	Number of Clusters (Default is 2): <input type = "text" name = "clsNu" size="3" /> <br>
		Convergence Delta (Default is 0.5): <input type = "text" name = "cnvDelta" size="5" /> <br>
		Maximum Number of Iterations (Default is 10): <input type = "text" name = "maxIter" size="5" /><br>
    	<input type="submit" name="dataAnalysis" value="KMeans">
    </form>
</body>
</html>